import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';

import { Subject, Observable } from 'rxjs';
import { take, finalize } from 'rxjs/operators';

import { Image } from 'src/app/core/models';
import { SearchFormComponent } from 'src/app/shared/components/search-form/search-form.component';
import { ImageService } from 'src/app/core/services/image/image.service';

@Component({
  selector: 'app-meme-generator',
  templateUrl: './meme-generator.component.html',
  styleUrls: ['./meme-generator.component.scss']
})
export class MemeGeneratorComponent implements OnInit {

  @ViewChild(SearchFormComponent) searchForm: SearchFormComponent;
  images: Observable<Image[]>;
  $searchEnd = new Subject();

  constructor(private imageService: ImageService) { }

  ngOnInit() {
  }

  searchImage(keyword: string) {
    this.clearValues();

    this.images = this.imageService.getImages(keyword)
      .pipe(
        finalize(() => this.$searchEnd.next()),
        take(1)
      );
  }

  clearValues() {
  }
}
