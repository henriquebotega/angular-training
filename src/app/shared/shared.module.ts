import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { ButtonComponent } from './components/button/button.component';
import { ImageGalleryModule } from './components/image-gallery/image-gallery.module';

@NgModule({
  declarations: [
    SearchFormComponent,
    ButtonComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ImageGalleryModule,
  ],
  exports: [
    ReactiveFormsModule,
    SearchFormComponent,
    ButtonComponent,
    ImageGalleryModule
  ],
})
export class SharedModule { }
